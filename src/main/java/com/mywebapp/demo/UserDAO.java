package com.mywebapp.demo;

import org.springframework.data.repository.CrudRepository;

public interface UserDAO extends CrudRepository<User,Integer> {

}
