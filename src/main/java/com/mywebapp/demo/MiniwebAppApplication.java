package com.mywebapp.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniwebAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiniwebAppApplication.class, args);
	}

}
