package com.mywebapp.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {
	
	@Autowired
	UserDAO userda;

	@RequestMapping("index")
	public String user() {
		return "index.jsp";
	}

	@RequestMapping("addUser")
	public String addUser(User user) {
		userda.save(user);
		System.out.println("Database connected");
		return "index.jsp";
	}

	@RequestMapping("getUser")
	public ModelAndView getUser(@RequestParam int id) {
		ModelAndView mav = new ModelAndView("showUser.jsp");
		User user = userda.findById(id).orElse(null);
		mav.addObject(user);
		return mav;
	}

	@RequestMapping("deleteUser")
	public ModelAndView deleteUser(@RequestParam int id) {
		ModelAndView mav = new ModelAndView("deleteUser.jsp");
		User user = userda.findById(id).orElse(null);
		userda.deleteById(id);
		mav.addObject(user);
		return mav;
	}

	@RequestMapping("updateUser")
	public ModelAndView updateUser(User user) {
		ModelAndView mav = new ModelAndView("updateUser.jsp");
		user = userda.findById(user.getId()).orElse(null);
		userda.deleteById(user.getId());
		mav.addObject(user);
		return mav;
	}

}



